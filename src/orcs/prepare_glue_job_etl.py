from __future__ import print_function
import boto3
import urllib
# glue_client = boto3.client('glue')

def lambda_handler(event, context):
    
    name = event["name"]
    params_data_date = event["params"]["data_date"]
    uri_list = event["params"]["uri_list"]

    print("name: ", name)
    print("params_data_date: ", params_data_date)
    print("uri_list: ", uri_list)